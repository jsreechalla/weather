import { Template } from 'meteor/templating';
import { _ } from 'meteor/underscore';
import { Factory } from 'meteor/dburles:factory';
import { Blaze } from 'meteor/blaze';
import { Tracker } from 'meteor/tracker';
import {$} from 'meteor/jquery';
import { chai } from 'meteor/practicalmeteor:chai';
import './main.js';
//import './main.html';

const withDiv =function withDiv(callback){
  const el = document.createElement('div');
  document.body.appendChild(el);
  try{
    callback(el);
  }
  finally{
    document.body.removeChild(el);
  }
}
const withRenderedTemplate = function withRenderedTemplate(template, data, callback) {
  withDiv((el) => {
    const ourTemplate = _.isString(template) ? Template[template] : template;
    Blaze.renderWithData(ourTemplate, data, el);
    Tracker.flush();
    callback(el);
  });
};
describe(' home', function () {
  beforeEach(function () {
    Template.registerHelper('_', key => key);
  });

  afterEach(function () {
    Template.deregisterHelper('_');
  });

   it('Renders correctly', function () {
  //   const help = Factory.build('display',{ checked:false });
  //   const data = {
  //     display:display._transform(display),
  //     //number:number._transform(number),
  //     onEditingChange:() => 0
  //   };

    withRenderedTemplate('home',{}, el => {
      //chai.assert.equal(display,help.display);
      //chai.assert.equal(number,help.number);
      //console.log($(el).find('input[type=submit]').on('click'));
      chai.assert.equal($(el).find('input[type=submit]').on('click').length,1);
});
});
});
// describe('home handlers',function(){
//    it("test helpers1",function(){
//   withRenderedTemplate('home',{},el =>{
//     chai.assert.equal(el.display,'');
//   })
//   })
// });
